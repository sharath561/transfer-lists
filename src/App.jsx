import React from 'react'
import { useState } from 'react'

const App = () => {

  let [list1, setList1] = useState({ "js": false, "HTML": false, "CSS": false, "TS": false })
  let [list2, setList2] = useState({ "React": false, "Angular": false, "Vue": false, "Spring": false })

  function RenderLists(list, setList) {

    return (

      Object.keys(list).map((item) =>
        <div key={item} className='m-5'>
          <input type='checkbox' className='mx-2'
            onChange={() => {
              let newList = {...list}
              newList[item] = !newList[item]
              setList(newList)
            }}
          />
          {" "}

          <label>{item}</label>
        </div>
      )
    )

  }

  function selectAll(list, updateList1, updateList2, prevList) {

    let updatePrevList = {}
    Object.keys(prevList).map((item) => {
      if (prevList[item]) {
        updatePrevList[item] = false

      }
      else {
        updatePrevList[item] = false
      }
    })

    updateList2({ ...list, ...updatePrevList })
    updateList1({})
  }

  function selectMove(list, updateList1, updateList2, prevList) {

    let oldList = {};
    Object.keys(list).filter((item) => {
      if (list[item] == false) {
        oldList[item] = false
      }
    })

    let newList = {}
    Object.keys(list).filter((item) => {
      if (list[item]) {
        list[item] = false
        newList[item] = false
      }
    })
    updateList1(oldList)
    updateList2({ ...prevList, ...newList })
  }

  function checkStatus(list){

    const result = Object.keys(list).some((item) => list[item] == true)
    return result

  }

  return (
    <div>
      <div className='text-2xl font-bold text-center mt-5'>
        Transfer List
      </div>

      <div className='flex m-10'>
        <div className='border-2 border-black w-[45%]'>
          {RenderLists(list1, setList1)}
        </div>
        <div className='flex flex-col  border border-black w-[8%] justify-between'>
          <button className='border border-black mx-5 my-3 bg-gray-200 disabled:bg-gray-500'  onClick={() => selectAll(list1, setList2, setList1, list2)}  disabled={Object.keys(list2).length === 0}>
            &lt;&lt;
          </button>

          <button className='border border-black mx-5 my-3 bg-gray-200 disabled:bg-gray-500'  onClick={() => selectMove(list1, setList1, setList2, list2)} disabled={!checkStatus(list1)}>
            &gt;
          </button>
          <button className='border border-black mx-5 my-3 bg-gray-200 disabled:bg-gray-500'  onClick={() => selectMove(list2, setList2, setList1, list1)} disabled={!checkStatus(list2)}>
            &lt;
          </button>

          <button className='border border-black mx-5 my-3 bg-gray-200 disabled:bg-gray-500'  onClick={() => selectAll(list2, setList1, setList2, list1)} disabled={Object.keys(list1).length === 0}>
            &gt;&gt;
          </button>
        </div>
        <div className='border-2 border-black w-[45%]'>
          {RenderLists(list2, setList2)}
        </div>

      </div>

    </div>
  )
}

export default App